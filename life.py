import curses
import gui
from world import World
from lifetest import life

@life(1)
def create_new_world(columns, rows):
	return World(columns, rows)

@life(2)
def survival(is_alive, neighbours):
	if is_alive and (neighbours == 2 or neighbours == 3):
		return True
	else:
		return False
		
@life(3)
def birth(is_alive, neighbours):
	if not is_alive and neighbours == 3:
		return True
	else:
		return False


def coordinates_exists(world, column, row):
	if column >= 0 and row >= 0 and column < world.get_num_columns() and row < world.get_num_rows():
		return True
	else:
		return False


@life(4)
def is_alive(world, column, row):
	if coordinates_exists(world, column, row):
		return world.is_alive(column, row)
	else:
		return False


@life(5)
def count_neighbours(world, column, row):
	neighbours_list = [(-1,-1), (-1, 0), (-1, 1),
                       ( 0,-1),          ( 0, 1),
                       ( 1,-1), ( 1, 0), ( 1, 1)]
	no_of_neighbours = 0
	for x, y in neighbours_list:
		if is_alive(world, column + x, row + y):
			no_of_neighbours += 1
	
	return no_of_neighbours


@life(6)
def live_die_or_birth(world, column, row, no_of_neighbours):
	"""
	if survival(is_alive(world, column, row), no_of_neighbours):
		return True
	elif birth(is_alive(world, column, row), no_of_neighbours):
		return True
	else:
		return False
	"""
	alive = is_alive(world, column, row)
	return survival(alive, no_of_neighbours) or birth(alive, no_of_neighbours)


@life(7)
def next_world(world):
	new_world = create_new_world(world.get_num_columns(), world.get_num_rows())
	for y in range(world.get_num_rows()):
		for x in range(world.get_num_columns()):
			if live_die_or_birth(world, x, y, count_neighbours(world, x, y)):
				new_world.alive(x, y)
			else:
				new_world.dead(x, y)

	return new_world



def main(stdscreen):
    game_gui = gui.conwayGUI(stdscreen)
    my_world = create_new_world(game_gui.get_window_x(), game_gui.get_window_y())
    my_world.populate()
    game_gui.update_game(my_world)
    
    while True:
        my_world = next_world(my_world)
        game_gui.update_game(my_world)

if __name__ == "__main__":    
    curses.wrapper(main)
