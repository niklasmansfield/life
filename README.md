 Life
======

Spelet "Life" eller "Conway's Game of Life" skapades av matematiken John Horton Conway och presenterades i tidningen "Scientific America" i oktober 1970. Spelet, eller tankeexperimentet, är en simulator över hur en koloni av enkla organismer kan utveckas. Det är ett "zero-player"-spel vilket betyder att man efter grunduppställningen inte kan ge någon input, utan spelet fortsätter av sig själv.

FILM

Spelvärlden är två-dimensionell och består av ett rutnät, precis som ett rutat kollegieblock. Spelvärlden har en bestämd storlek, vilket gör att vi måste hålla reda på kanten och hörnorna.

Varje ruta har åtta grannrutor, varje ruta längs kanten har fem grannrutor och i hörnorna tre grannrutor:

Bild:

	mitten		  kanten	   hörn
	 123			23			
	 4*5        	*5 			*1
	 678			78			23

Varje ruta kan innehålla en individ eller vara tom.

# De tre reglerna:

Det finns tre regler som beskriver hur spelet går till.

1. **Överlevnad:** Varje individ med två eller tre individer i grannrutorna överlever till nästa omgång
1.  **Födsel:** Det uppstår en ny individ i varje tom ruta som har tre individer i grannrutorna
1. **Död:** Om de två reglerna ovan inte inträffar ska rutan i fråga vara tom

Alla rutor ska uppdateras samtidigt vilket betyder att man behöver komma ihåg två världar parallellt. En värld som är "nuet" och en som är "framtiden" där man fyller i hur nästa omgång ser ut enligt de tre reglerna. Spelet går ut på att se hur en befolkning utvecklar sig över tiden. Man kan säga att det är en datorsimulering över en koloni bakterier.


# Starta

Börja med att klona life-gitten ifrån bitbucket [https://bitbucket.org/patwic/life](https://bitbucket.org/patwic/life).

I den här modulen kommer du att skriva kod som gör att kolonin utvecklas enligt de tre reglerna. I extrauppgifterna kommer övningar som gör att du kan ändra startuppställningen, räkna individerna och lite annat.



# Nedbrytning av spelet i delar
* Vi har nuvarande värld med individ och vi behöver en ny tom värld där vi kan fylla i hur nästa spelomgång ska se ut.
* För att veta var vi ska placera ut invånarna i den nya världen behöver vi använda de tre spelreglerna.
* Vi måste använda de tre reglerna på varje enskild ruta i den nuvarande världen för att räkna ut hur den rutan ska se ut i nästa spelomgång.
* Vad behöver vi veta för att kunna använda de tre reglerna?
	- Om det finns en individ eller inte i rutan i nuvarande spelomgång.
	- Antal individer i grannrutorna.
* Vad behöver vi veta för att få reda på antal individer i grannrutorna?
	- Koordinater till de åtta grannrutorna.
	- Hantera kanter och hörnor så att antal grannar räknas på rätt sätt.
	- Avläsning av grannrutorna och summering av antal individer i dom.


# Övningar

## Skapa en ny värld
* Öppna filen `life.py` i geany. Den är ganska tom just nu, men här ska du själv skriva funktionerna som hanterar rutor, kanter, räknar individer och använder de olika reglerna.

    	import curses
	    import gui
	    from world import World

        from lifetest import life

	    # Här ska du skriva dina funktioner

	    def main(stdscreen):
		    game_gui = gui.conwayGUI(stdscreen)
		    my_world = None # fyll i ett funktionsanrop här
    
            game_gui.update_game(my_world)

	    curses.wrapper(main)

* De första tre raderna importerar färdigskriva funktioner och bibliotek som vi behöver använda. Variabeln game_gui (GUI: Graphic User Interface) hanterar skärmen och har en funktion som heter game_gui.update_game() som du använder för att uppdatera skärmen mellan spelomgångarna.
* För att du ska veta hur stor världen är finns det i klassen conwayGUI två funktioner som heter `get_window_x()` och `get_window_y()`. Dessa funktioner returnerar storleken på fönstret innanför ramen som är vår tillgängliga spelplan.
* `from world import World` importerar klassen World som är objektet som håller reda på vårt rutnät och alla individer. Objekt som är av typen World har några funktioner som kan vara bra att känna till.
	- `get_num_cols()` och `get_num_rows()` returnerar antal kolumner respektiver rader för världen.
	- `is_alive(kolumn, rad)` returnerar ett boolskt värde som berättar ifall individen i ruta (kolumn, rad) lever eller ej.
	- `alive(kolumn, rad)` och `dead(kolumn, rad)` som sätter värdet True respektive False för den aktuella rutan.
* Definiera en funktion som skapar en ny värld. Som inargument ska funktionen ta antal rader och kolumner och funktionen ska returnera ett objekt av klassen World. För att skapa en ny värld anropar vi `World(kolumner, rader)`.
* Fyll sedan i definitionen av `my_world` med ett anrop till din funktion, så att det blir ett World-objekt med samma storlek som fönstret. Kom ihåg att storleken på fönstret kan fås genom `game_gui.get_window_x()` och `game_gui.get_window_y()`.
* Du kan nu provköra programmet med:

        python3 life.py
* Avsluta genom att trycka på valfri tangent.
* För att det ska bli lite roligare så kan du lägga in en population från start. Det gör du genom att lägga till

        my_world.populate(["  ***", " *  *", "*   *"])

* Provkör igen! Du ska nu se följande mönster:

     ````
       *** 
      *  * 
     *   *
     ````

Nu är du redo att börja skriva logiken som sätter liv i Life!

## För att testa din funktion
* För att kunna kontrollera att funktionen du just skrev är rätt ska du på raden ovanför din funktion skriva `@life(1)`. Det gör att du kan köra Patwics tester som berättar för dig om du har gjort rätt och kan gå vidare eller om du måste jobba vidare med funktionen.
* För att testa funktionen så skriver du i kommandotolken

	`grader life.py life.1`
    

## De tre reglerna
* Skriv de två reglerna *Överlevnad* och *Födsel* i två olika funktioner som tar in två argument
	- har nuvarande ruta en invånare eller ej
	- antal individer i grannrutorna
* Funktionerna ska returna **True** om regeln är uppfylld och annars **False**.
* Funktionen som tar hand om *Döden* hanteras lättast som en `else`-sats till de andra två reglerna.
* Dekorera överlevnadsregeln med `@life(2)` och födelseregeln med `@life(3)`för att kunna testa att du skrivit dem rätt. Testa genom att skriva `grader life.py life.2` respektive `grader life.py life.3` i kommandotolken.



## Är rutan bebodd eller inte?
* Skriv en funktion som returnerar **True** om en ruta har en individ och **False** annars. Som inargument behövs världen och två koordinater för att peka ut vilken ruta som menas.
* Här behövs kontrolleras att rutan finns. Tanken är att använda denna funktion när vi ska ta reda på hur många grannar en ruta har och då måste funktionen fungera längs kanterna i hörnen också. Om koordinaterna pekar på en ruta utanför världen ska False returneras.
* Med *utanför världen* menas när koordinaterna är mindre än 0 eller större än antalet kolumner respektive rader. Det går alldeles utmärkt att flytta ut den här kontrollen till en egen funktion som sedan anropas från den första.
* Dekorera funktionen med `@life(4)` och kör grader-verktyget för att kontrollera om du har gjort rätt.




## Hantera kanterna
* Här kan du testköra lite kod som försöker visa problematiken med kanterna.

Det första exemplet går ut på att visa att man måste vara uppmärksam på koordinaterna jämfört med antal rader. Om `world.get_num_rows()` returnerar 8 så är den sista raden som finns rad nummer 7.

Exemplet kommer att fallera då det försöker accessa en ruta som inte finns.

<iframe src="http://pythontutor.com/iframe-embed.html#code=%0A%23+A+very+small+%22world%22%0A%23+filled+with+letters%0A%23+to+visualize.%0Aw+%3D+%5B%5B'a',+'b',+'c'%5D,+%0A+++++%5B'd',+'e',+'f'%5D,+%0A+++++%5B'g',+'h',+'i'%5D,+%0A+++++%5B'j',+'k',+'l'%5D%5D%0A%0Arows+%3D+len(w)%0Acolumns+%3D+len(w%5B0%5D)%0A%23+Note%3A+it+is+w%5Brow%5D%5Bcolumn%5D%0Aw00+%3D+w%5B0%5D%5B0%5D%0Aw11+%3D+w%5B1%5D%5B1%5D%0A%0A%23+The+next+line+won't+work!%0Aw43+%3D+w%5B4%5D%5B3%5D%0A&amp;cumulative=false&amp;heapPrimitives=false&amp;drawParentPointers=false&amp;textReferences=false&amp;showOnlyOutputs=false&amp;py=3&amp;curInstr=4&amp;codeDivWidth=350&amp;codeDivHeight=500" width="900" height="650"></iframe>
    
http://pythontutor.com/visualize.html#code=%0A%23+A+very+small+%22world%22%0A%23+filled+with+letters%0A%23+to+visualize.%0Aw+%3D+%5B%5B'a',+'b',+'c'%5D,+%0A+++++%5B'd',+'e',+'f'%5D,+%0A+++++%5B'g',+'h',+'i'%5D,+%0A+++++%5B'j',+'k',+'l'%5D%5D%0A%0Arows+%3D+len(w)%0Acolumns+%3D+len(w%5B0%5D)%0A%23+Note%3A+it+is+w%5Brow%5D%5Bcolumn%5D%0Aw00+%3D+w%5B0%5D%5B0%5D%0Aw11+%3D+w%5B1%5D%5B1%5D%0A%0A%23+The+next+line+won't+work!%0Aw43+%3D+w%5B4%5D%5B3%5D%0A&mode=display&cumulative=false&heapPrimitives=false&drawParentPointers=false&textReferences=false&showOnlyOutputs=false&py=3&curInstr=4

Det andra exemplet visar vad som händer om man använder negativa koordinater.

<iframe width="900" height="700" frameborder="0" src="http://pythontutor.com/iframe-embed.html#code=%0A%23+A+very+small+%22world%22+%0A%23+filled+with+letters%0A%23+to+visualize.%0Aw+%3D+%5B%5B'a',+'b',+'c'%5D,+%0A+++++%5B'd',+'e',+'f'%5D,%0A+++++%5B'g',+'h',+'i'%5D,%0A+++++%5B'j',+'k',+'l'%5D%5D%0A%0Arows+%3D+len(w)%0Acolumns+%3D+len(w%5B0%5D)%0A%23+Note%3A+it+is+w%5Brow%5D%5Bcolumn%5D%0Aw00+%3D+w%5B0%5D%5B0%5D%0Aw02+%3D+w%5B0%5D%5B2%5D%0Aw32+%3D+w%5B3%5D%5B2%5D%0A%0A%23+These+kinda+works,+%0A%23+since+-1+wraps.%0Awm1m1%3D+w%5B-1%5D%5B-1%5D%0Aw0m1%3D+w%5B0%5D%5B-1%5D&amp;cumulative=false&amp;heapPrimitives=false&amp;drawParentPointers=false&amp;textReferences=false&amp;showOnlyOutputs=false&amp;py=3&amp;curInstr=4&amp;codeDivWidth=350&amp;codeDivHeight=500"> </iframe>

http://pythontutor.com/visualize.html#code=%0A%23+A+very+small+%22world%22+%0A%23+filled+with+letters%0A%23+to+visualize.%0Aw+%3D+%5B%5B'a',+'b',+'c'%5D,+%0A+++++%5B'd',+'e',+'f'%5D,%0A+++++%5B'g',+'h',+'i'%5D,%0A+++++%5B'j',+'k',+'l'%5D%5D%0A%0Arows+%3D+len(w)%0Acolumns+%3D+len(w%5B0%5D)%0A%23+Note%3A+it+is+w%5Brow%5D%5Bcolumn%5D%0Aw00+%3D+w%5B0%5D%5B0%5D%0Aw02+%3D+w%5B0%5D%5B2%5D%0Aw32+%3D+w%5B3%5D%5B2%5D%0A%0A%23+These+kinda+works,+%0A%23+since+-1+wraps.%0Awm1m1%3D+w%5B-1%5D%5B-1%5D%0Aw0m1%3D+w%5B0%5D%5B-1%5D&mode=display&cumulative=false&heapPrimitives=false&drawParentPointers=false&textReferences=false&showOnlyOutputs=false&py=3&curInstr=4

Det ser ut som om det fungerar, men notera värdet i wm1m1. Det är samma som w32. Och w02 är samma som w0m1. Med andra ord måste vi hålla koll på koordinaterna för att världen inte ska "wrappa" i negativa riktningar eller.
 

## Räkna grannarna
* Nu behövs en funktion som returnerar antal individer i en rutas grannrutor. Som inargument behövs världen och koordinater till rutan. Varje ruta kan ha upp till åtta grannrutor och för att hålla reda på alla grannarna kan man använda matrisen nedan. Till exempel har grannrutan snett ovanför till vänster om en ruta koordinaterna (x-1, y-1) och grannrutan rakt nedanför har koordinaterna (x, y+1).

	    # The relative neighbouring coordinates (x, y) at any given point.
        neighbours_list = [(-1,-1), ( 0,-1), ( 1,-1),
		    		       (-1, 0),          ( 1, 0),
			    		   (-1, 1), ( 0, 1), ( 1, 1)]

* Använd den här matrisen när du i tur och ordning anropar funktionen från förra uppgiften (den som du dekorerade med `@life(4)`) för att räkna ihop grannrutornas individer.
* Dekorera den här funktionen med `@life(5)` och testkör den.



## Räkna grannar och använd reglerna
Nu ska vi skriva en funktion som använder de tre reglerna och funktionen som räknar antal individer i grannrutorna. Funktionens inargument ska vara `def function_name(world, column, row, num_neighbours)` där num_neighbours är antal grannar till rutan med koordinater (column, row). Funktionen ska returnera **True** ifall den aktuella rutan ska vara bebodd i nästa spelomgång och False annars.

De tre reglerna modelleras lättast genom en `if-elif-else`-sats. Där testar vi först regel **Överlevnad** `@life(2)` och annars testar vi regel **Födsel** `@life(3)` och annars inträffar den tredje regeln, **Döden**, automatiskt.

Om du vill testa funktionen kan du använda `@life(6)`.


## En sista funktion
Nu behövs en funktion som tar nuvarande värld som inargument och som returnerar nästa värld som utargument. I den här funktionen ska du alltså gå igenom samtliga rutor i världen och ta reda på ifall de ska vara bebodda eller inte under nästa spelomgång. Dekorera funktionen med `@life(7)`för att kunna testa den.

* Börja med att anropa din första funktion `@life(1)` för att skapa en ny värld.

* Sedan behövs det två nästlade [for-loopar](https://canvas.swftw.se/courses/1/wiki/for?module_item_id=77). De ska räkna mellan 0 och antal rader respektive kolumner för att på så sätt gå igenom hela världen.

	    for x in ...:
	    	for y in ...:
		    	kod

* Inuti den innersta for-loopen behövs tre anrop:
	- Ta reda på antal grannar genom att anropa funktionen som du dekorerade med `@life(5)`.
	- Sen anropar du funktionen dekorerad med `@life(6)`.
	- Det tredje anropet är till antingen `alive(x, y)` eller `dead(x, y)` som finns i klassen World, för att ange ifall rutan ska vara bebodd eller inte i nästa spelomgång.
	
* Glöm inte att returnera den nya världen.


## Nu ska vi spela hela spelet
* Det sista som behövs är en [evighetsloop](https://canvas.swftw.se/courses/1/wiki/while?module_item_id=20) för att hela tiden gå igenom stegen:
	- uppdatera världen till nästa spelomgång, `@life(7)`
	- uppdatera skärmen med den nya världen, `gui.update_game(my_world)`

* Kör spelet! Nu ska mönstret förändras för varje varv som evighetsloopen körs. Du måste trycka ner en tangent mellan varje spelomgång och vill du avbryta spelet så trycker du `Ctrl + C`.


# Extrauppgifter

   * Gör en inmatningsrutin som skickar med ett start mönster till `my_world.populate()`.
     Formatet som `populate()` använder ser ut så här:

     `pattern = ["  ***", " *  *", "*   *"]`

     tex:

     `my_world.populate(pattern = ["  ***", " *  *", "*   *"])`

     Tips: Använd `input()` och en `while`-loop tills det kommer en tom rad och anta att inmatnigen är slut.

   * Gör en funktion som räknar ut antalet individer i världen och skicka med till `terminal_gui.update_game()`.

     ex:
     `terminal_gui.update_game(my_world, population = current_population)`

     där `current_population` är vad din funktion har räknat fram.

     Annotera funktionen med `@life(8)` och testa.

   * Skriv en funktion som kontrollerar om nuvarande och nästa värld är lika, och i så fall
     returnerar `True`, annars `False`
     
     tex:

     `if nothing_happens(world, next_world):`
     `	 break`

     Annotera funktionen med `@life(9)` och testa.
 









